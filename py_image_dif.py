import os
import timeit
import warnings

import matplotlib.pyplot as plt
import numpy as np
import psutil
from skimage.color import rgb2grey
from skimage.io import imread, imsave
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

warnings.filterwarnings('ignore')

# provided clustering methods
clustering_methods = (
    'KMeans', 'Agglomerative ward', 'Agglomerative complete', 'Agglomerative average', 'Agglomerative single')


class ImageDiffer:
    def __init__(self, number_of_differences, p_filter=0.1, clustering_method='Agglomerative average',
                 clustering=None, classifier=None):
        self.p_filter = p_filter
        self.number_of_differences = number_of_differences
        self.clustering_method = clustering_method
        self.clustering = clustering
        self.classifier = classifier
        self.clusters = []
        self.images = dict()

    def __init_images(self, image1, image2):
        if isinstance(image1, str):
            self.images['image1'] = imread(image1)
        else:
            self.images['image1'] = image1
        if isinstance(image2, str):
            self.images['image2'] = imread(image2)
        else:
            self.images['image2'] = image2

        grey_im_1 = rgb2grey(self.images['image1'])
        grey_im_2 = rgb2grey(self.images['image2'])
        self.images['dif'] = grey_im_2 - grey_im_1

        # The choice of filter
        if self.p_filter == 'color':
            coordinates = self.__color_clustering()[0]
        elif isinstance(self.p_filter, float) and 0 <= self.p_filter < 1:
            coordinates = self.__absolute_dif()
        else:
            raise AttributeError('Invalid p_filter: {}'.format(self.p_filter))
        return coordinates

    # Color filter
    def __color_clustering(self, n_clusters=2):
        h, w = tuple(self.images['dif'].shape)
        l_mi = self.images['dif'].copy().reshape(w * h, 1)  # 1D reshape

        km = KMeans(n_clusters=n_clusters, init='k-means++')
        km.fit(l_mi)

        # Build a cluster dictionary
        colors_clusters = dict()
        l = len(l_mi)
        for i in range(n_clusters):
            colors_clusters[i] = []  # Create list in dictionary
        for i in range(l):
            colors_clusters[km.labels_[i]].append(list(l_mi[i]))  # Filling the clusters of pixels

        # Create coordinate list
        coord_clusters = dict()
        for i in range(n_clusters):
            coord_clusters[i] = np.where(km.labels_ == i)

        s_cl_coords = sorted(coord_clusters.items(), key=lambda a: len(a[1]), reverse=True)

        # Set coordinates
        coord_clusters_2d = []
        for i in s_cl_coords[1:]:
            coord_2d = np.zeros((len(i[1][0]), 2))
            for ij, j in enumerate(i[1][0]):
                coord_2d[ij][0] = j // w
                coord_2d[ij][1] = j % w
            coord_clusters_2d.append(coord_2d)

        return coord_clusters_2d

    # Proportional filter
    def __absolute_dif(self):
        coordinates = []
        mask = (np.abs(self.images['dif']) > self.p_filter)
        for i in range(mask.shape[0]):
            for j in range(mask.shape[1]):
                if mask[i][j]:
                    coordinates.append([i, j])
        return np.array(coordinates)

    def fit(self, image1, image2, result_name=None, mark=True):
        coordinates = self.__init_images(image1, image2)

        # Set adaptive clustering size
        stats = psutil.virtual_memory()  # returns a named tuple
        available = getattr(stats, 'available')
        free_GB = available / 10 ** 9
        size = round(free_GB * 2500 / len(coordinates), 2)
        if size > 1:
            size = 0.97

        # Set clustering
        X_train, X_test = train_test_split(coordinates, train_size=size)
        if self.clustering is None:
            if self.clustering_method == 'KMeans':
                self.clustering = KMeans(n_clusters=self.number_of_differences)
            elif self.clustering_method == 'Agglomerative' or self.clustering_method == 'Agglomerative ward':
                self.clustering = AgglomerativeClustering(n_clusters=self.number_of_differences)
            elif self.clustering_method == 'Agglomerative complete':
                self.clustering = AgglomerativeClustering(n_clusters=self.number_of_differences, linkage='complete')
            elif self.clustering_method == 'Agglomerative average':
                self.clustering = AgglomerativeClustering(n_clusters=self.number_of_differences, linkage='average')
            elif self.clustering_method == 'Agglomerative single':
                self.clustering = AgglomerativeClustering(n_clusters=self.number_of_differences, linkage='single')
            else:
                raise AttributeError('Invalid method name ' + self.clustering_method)

        print(X_train)
        self.clustering.fit(X_train)

        # Set classifier
        if self.classifier is None:
            self.classifier = DecisionTreeClassifier(max_depth=10)
        self.classifier.fit(X_train, self.clustering.labels_)
        labels = self.classifier.predict(coordinates)
        # Merging coordinates with labels
        self.clusters = np.hstack([coordinates, labels.reshape(len(labels), 1)])

        # Draw cluster
        tr = np.zeros(self.images['image1'].shape, dtype=int)
        colors = [[np.random.randint(0, 255) for x in range(3)] for y in range(self.number_of_differences)]
        for coord in self.clusters:
            tr[int(coord[0])][int(coord[1])] = colors[int(coord[2])]
        if result_name is None:
            self.images['Coordinate clustering'] = tr
        else:
            self.images[result_name] = tr

        if mark:
            self.draw_marks()

    def methods_test(self, image1, image2, methods=clustering_methods, show=False):
        for i in methods:
            a = timeit.default_timer()
            self.fit(image1, image2, result_name=i, mark=False)
            if show:
                print(i + ' time: ' + str(timeit.default_timer() - a))

    def draw_marks(self, line_width=None):
        marker_points = []
        for i in np.unique(self.clusters[:, 2]):
            i_cluster = np.array(list(filter(lambda a: a[2] == i, self.clusters)), dtype=int)
            p1 = (i_cluster[:, 0].min(), i_cluster[:, 1].min())
            p2 = (i_cluster[:, 0].max(), i_cluster[:, 1].max())
            marker_points.append((p1, p2))

        # Adaptive line_width
        if line_width is None:
            line_width = 10
            min_diag = min([(i[1][0] - i[0][0]) + (i[1][1] - i[0][1]) for i in marker_points])
            if min_diag < line_width * 3:
                line_width = min_diag // 2 + 1

        colors = [[np.random.randint(0, 255) for x in range(3)] for y in range(len(marker_points))]

        for iim, im in enumerate((self.images['image1'], self.images['image2'])):
            tr = im.copy()
            for idiag, diag in enumerate(marker_points):
                for i in range(line_width):
                    for k in range(diag[0][0], diag[1][0] + 1):
                        tr[k][diag[0][1] + i] = colors[idiag]
                        tr[k][diag[1][1] - i] = colors[idiag]
                    for k in range(diag[0][1], diag[1][1] + 1):
                        tr[diag[0][0] + i][k] = colors[idiag]
                        tr[diag[1][0] - i][k] = colors[idiag]
            self.images['mark_{}'.format(iim + 1)] = tr

    def save_images(self, name=''):
        if name != '':
            path = str(os.getcwd()) + '\\out\\{}'.format(name)
            try:
                os.mkdir(path)
            except OSError:
                print("Создать директорию %s не удалось" % path)
        else:
            path = str(os.getcwd()) + '\\out'

        for i in self.images.items():
            imsave(path + '\\{}.jpg'.format(i[0]), i[1])

    def im_print(self):
        l = len(self.images)
        plt.rcParams['figure.figsize'] = (20, 15 * l)  # Set canvas size for fullscreen view
        fig, axes = plt.subplots(l, 1)  # Create figures for images
        for i, im in enumerate(self.images.items()):
            axes[i].set_axis_off()
            axes[i].set_title('{}'.format(im[0]))
            axes[i].imshow(im[1])
